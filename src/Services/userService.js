import axios from 'axios';
import openSocket from 'socket.io-client';

const serverIP = 'http://51.77.137.116:8082';
const socket = openSocket('51.77.137.116:8082');


function getSocket() {
  return (socket);
}
/*
inscription (user_name, email, password)
-> {data: {status: "bad param", code: 400, format: "JSON"}}
-> {data: {status: "email already taken", code: 401, format: "JSON"}}
-> {data:{status:"ok", code:200, format:"JSON"}}
*/
function login(email, password) {
  const user = { email };
  return axios.post('http://51.77.137.116:8082/connexion', {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    },
    email,
    password,
  }).then((response) => {
    if (response.data.data.code === 200) {
      socket.emit('new user', { email });
      localStorage.setItem('user', JSON.stringify(response.data.user));
      return user;
    }
    return null;
  }).catch((error) => {
    throw error;
  });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}

/*
inscription (user_name, email, password)
-> {data: {status: "bad param", code: 400, format: "JSON"}}
-> {data: {status: "email already taken", code: 401, format: "JSON"}}
-> {data:{status:"ok", code:200, format:"JSON"}}
*/

function register(user) {
  return axios.post(`${serverIP}/inscription`, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    user_name: user.firstname,
    email: user.email,
    password: user.password,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
}

export const userService = {
  login,
  logout,
  register,
  getSocket,
};
