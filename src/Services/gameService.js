import axios from 'axios';
import { userService } from './userService';

const serverIP = 'http://51.77.137.116:8082';

/* /room/get(email)
-> {data: {status: "bad param", code: 400, format: "JSON"}}
-> {data:{status:"ok",code:200,format:"JSON"},
    result:[{game_name:"game",game_status:2},
        {game_name:"pedro",game_status:1},
        {game_name:"json",game_status:1}]}
*/

function getRooms() {
  const user = JSON.parse(localStorage.getItem('user'));
  return axios.post(`${serverIP}/room/get`, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    email: user.email,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
}

function gameStart(gameName) {
  return axios.post(`${serverIP}/game/start`, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    game_name: gameName,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
}

/* /room/create(email, game_name)
-> {data: {status: "bad param", code: 400, format: "JSON"}}
-> {data: {status: "game name already taken", code: 401, format: "JSON"}}
-> {data:{status:"ok", code:200, format:"JSON"}} */

function createRoom(user, gameName) {
  userService.getSocket().on('join', () => {
    gameStart(gameName).then((response) => {
      if (response.data.code === 200) {
        userService.getSocket().on('start', () => {
        });
      }
    });
  });
  return axios.post(`${serverIP}/room/create`, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    email: user.email,
    game_name: gameName,
  }).then(response => response.data)
    .catch((error) => {
      throw error;
    });
}

/*
/room/join(email, game_name)
-> {data: {status: "bad param", code: 400, format: "JSON"}}
-> {data: {status: "game is full", code: 401, format: "JSON"}}
-> {data: {status: "game not found", code: 404, format: "JSON"}}
-> {data:{status:"ok", code:200, format:"JSON"}}
*/

function joinRoom(user, gameName) {
  userService.getSocket().on('start', () => {
  });
  return axios.post(`${serverIP}/room/join`, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    email: user.email,
    game_name: gameName,
  }).then(response => response.data).catch((error) => {
    throw error;
  });
}

export const gameServices = {
  getRooms,
  createRoom,
  joinRoom,
};
