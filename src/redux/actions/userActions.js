import { userConstants } from '../constants/action-type';
import { userService } from '../../Services/userService';
import createBrowserHistory from '../../Helpers/history';


function login(email, password) {
  function request(user) { return { type: userConstants.LOGIN_REQUEST, user }; }
  function success(user) { return { type: userConstants.LOGIN_SUCCESS, user }; }
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error }; }
  return (dispatch) => {
    dispatch(request({ email, password }));
    userService.login(email, password)
      .then(
        (user) => {
          if (user) {
            dispatch(success(user));
            createBrowserHistory.push('/home');
          } else {
            dispatch(failure('Wrong password'));
            createBrowserHistory.push('/login');
          }
        },
        (error) => {
          dispatch(failure(error.toString()));
        },
      );
  };
}

function logout() {
  userService.logout();
  createBrowserHistory.push('/login');
  return { type: userConstants.LOGOUT };
}

function register(user) {
  function request(userConnected) {
    return { type: userConstants.REGISTER_REQUEST, userConnected };
  }
  function success(userConnected) {
    return { type: userConstants.REGISTER_SUCCESS, userConnected };
  }
  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
  return (dispatch) => {
    dispatch(request(user));
    userService.register(user)
      .then(
        () => {
          dispatch(success());
          createBrowserHistory.push('/login');
          // dispatch(alertActions.success('Registration successful'));
        },
        (error) => {
          dispatch(failure(error.toString()));
          // dispatch(alertActions.error(error.toString()));
        },
      );
  };
}
export const userActions = {
  login,
  logout,
  register,
};
