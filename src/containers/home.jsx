import React, { Component } from 'react';
import { connect } from 'react-redux';
import { gameServices } from '../Services/gameService';
import RoomsList from '../components/RoomsList';
import createBrowserHistory from '../Helpers/history';
import { userActions } from '../redux/actions/userActions';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameName: null,
      games: [],
    };
  }

  componentDidMount() {
    return gameServices.getRooms().then((rooms) => {
      this.setState({ games: rooms.result });
    });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  refreshGames() {
    return gameServices.getRooms().then((rooms) => {
      this.setState({ games: rooms.result });
    });
  }

  createRoom(email, gameName) {
    return gameServices.createRoom(email, gameName).then((response) => {
      if (response.data.code === 401) {
        // TODO pop up erreur
        console.log(response.data.status);
      } else if (response.data.code === 400) {
        console.log(response.data.status);
      } else {
        const game = `?game_name=${gameName}`;
        createBrowserHistory.push({ pathname: '/game', search: game });
      }
    });
  }

  render() {
    return (
      <div style={{ position: 'relative' }}>
        <h2>Bienvenue</h2>
        <button className="btn btn-danger" style={{ position: 'absolute', right: '2%' }} onClick={() => this.props.dispatch(userActions.logout())}>Se déconnecter</button>
        <button className="btn btn-primary" style={{ marginBottom: '1%' }} type="button" onClick={() => this.refreshGames()}>Rafraichir</button>
        <form>
          <input
            onChange={ (e) => this.onChange(e) }
            name="gameName"
            type="text"
            placeholder="Nom de la partie"
            required
          />
          <button className="btn-primary" style={{ marginLeft: '10px' }} type="button" onClick={() => this.createRoom(JSON.parse(localStorage.getItem('user')), this.state.gameName)}>Créer une partie</button>
        </form>
        <div className="col-md-12">
          {this.state.games.length > 0 ? <RoomsList style={{ marginTop: '2%' }} rooms={this.state.games} /> : <div>Aucune partie en attente veuillez en créer une</div>}
        </div>

      </div>
    );
  }
}

function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
    user,
    users,
  };
}

export default connect(mapStateToProps)(Home);
