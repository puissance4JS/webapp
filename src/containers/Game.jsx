import React, { Component } from 'react';
import queryString from 'query-string';
import Board from '../components/Board';
import { userService } from '../Services/userService';

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameStatus: 0,
      player: JSON.parse(localStorage.getItem('user')).user_id,
      gameName: null,
    };
  }

  componentDidMount() {
    userService.getSocket().on('launch game', (data) => {
      console.log(data);
    });
    this.setState({ gameName: queryString.parse(window.location.search) });
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <Board player={this.state.player} game={queryString.parse(window.location.search)} />
        </div>
      </div>
    );
  }
}

export default Game;
