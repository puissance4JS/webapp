import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  Router,
  Route,
  Redirect,
} from 'react-router-dom';
import store from './Helpers/store';
import Home from './containers/home';
import Game from './containers/Game';
import Login from './containers/Login';
import Lobby from './containers/Lobby';
import Register from './containers/Register';
import createBrowserHistory from './Helpers/history';


export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      localStorage.getItem('user')
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )}
  />
);

const SpecialRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={() => (
      localStorage.getItem('user')
        ? <Redirect to="/home" />
        : <Redirect to="/login" />
    )}
  />
);

const Root = () => (
  <Provider store={store}>
    <Router history={createBrowserHistory}>
      <div>
        <SpecialRoute exact path="/" component={null} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <PrivateRoute exact path="/home" component={Home} />
        <PrivateRoute exact path="/game" component={Game} />
        <PrivateRoute exact path="/lobby" component={Lobby} />
      </div>
    </Router>
  </Provider>
);
ReactDOM.render(<Root />, document.getElementById('root'));
module.hot.accept();
