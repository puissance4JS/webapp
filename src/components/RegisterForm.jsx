import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../redux/actions/userActions';

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        firstname: '',
        lastname: '',
        email: '',
        password: '',
      },
      submitted: false,
    };
  }

  onChange(e) {
    const { name, value } = e.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value,
      },
    });
  }

  onSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    if (user.email && user.password) {
      dispatch(userActions.register(user));
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4 col-md-offset-4">
              <h1 className="text-center login-title">Inscrivez-vous !</h1>
              <div className="account-wall">
                <img
                  className="profile-img"
                  src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                  alt=""
                />
                <form className="form form-signin" onSubmit={e => this.onSubmit(e)}>
                  <input
                    onChange={e => this.onChange(e)}
                    name="lastname"
                    type="text"
                    className="form-control"
                    placeholder="Nom"
                    required
                    autoFocus
                  />
                  <input
                    onChange={e => this.onChange(e)}
                    name="firstname"
                    type="text"
                    className="form-control"
                    placeholder="Prénom"
                    required
                    autoFocus
                  />
                  <input
                    onChange={e => this.onChange(e)}
                    name="email"
                    type="text"
                    className="form-control"
                    placeholder="Email"
                    required
                    autoFocus
                  />
                  <input
                    onChange={e => this.onChange(e)}
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    required
                  />
                  <button className="btn btn-primary" type="submit">S'enregistrer</button>
                </form>
              </div>
              <Link to="/login">Connectez-vous</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
    registering,
  };
}

export default connect(mapStateToProps)(RegisterForm);
