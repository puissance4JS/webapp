import React, { Component } from 'react';
import '../Style/roomItem.css';
import createBrowserHistory from '../Helpers/history';
import { gameServices } from '../Services/gameService';


class RoomsList extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
  }

  joinRoom = (game_name) => {
    return gameServices.joinRoom(JSON.parse(localStorage.getItem('user')), game_name).then((response) => {
      if (response.data.code === 200) {
        let game = '?game_name=' + game_name;
        createBrowserHistory.push({pathname: '/game', search:game})
      } else {
        console.log(response.data);
      }
    })
  };

  render() {
    return (
      <div>
        { this.props.rooms.map(room => (
          <li className={'roomItem col-md-3'} >
            {room.game_name}
            {room.game_status === 1 ?
                <div><p>Partie en attente</p>
                  <button className={'btn btn-info'} type='button' onClick={() => this.joinRoom(room.game_name)} >Rejoindre la partie</button><br/></div> :
                <p>Partie en cours</p>}
          </li>
        ))}
      </div>
    );
  }
}

export default RoomsList;
