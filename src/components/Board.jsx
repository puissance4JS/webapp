import React, { Component } from 'react';
import uuid from 'uuid/v4';
import { userService } from '../Services/userService';


class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      board: [[0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0]],
      idCurrentPlayer: null,
      gameName: null,
    };
  }

  componentDidMount() {
    let self = this;
    userService.getSocket().on("play", function(data) {
      self.handleData(data)
    });
    this.setState({gameName: this.props.game.game_name});
    this.setState({idCurrentPlayer:this.props.player})
  }


  handleData(data) {
    let copyBoard = this.state.board;
    let lineBoard = 5;
    data.forEach((line) => {
    Object.entries(line).forEach((value, i) => {
      copyBoard[lineBoard][i] = value[1];
    });
    lineBoard--;
    });
    if(lineBoard === -1){
      this.setState({board:copyBoard});
      this.forceUpdate()
    }
  };

  handleClick(x, y) {
      userService.getSocket().emit('play', {user_id:this.props.player, game_name:this.props.game.game_name, col: x, line: y});

  }

  displayRow = (row, y) => {
    const table = [];
    for (let x = 0; x < row.length; x++) {
      let key = uuid();
      if (x + 1 === row.length) {
        if(row[x] === 1){
            table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseJaune.png')} />);
            table.push(<br />);
        } else if (row[x] === 2) {
            table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseRouge.png')} />);
            table.push(<br />);
        } else {
            table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseVide.png')} />);
            table.push(<br />);
        }

      } else {
        if(row[x] === 1){
              table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseJaune.png')} />);
          } else if (row[x] === 2) {
              table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseRouge.png')} />);
          } else {
              table.push(<img onClick={() => this.handleClick(x, y)} alt={''} key={key} src={require('../assets/caseVide.png')} />);
          }
      }
    }
    return table;
  };

  render() {
    return (
      <div className="col-md-12" style={{ width: '100%' }}>
        {this.state.board.map((row) => this.displayRow(row))}
      </div>
    );
  }
}

export default Board;
