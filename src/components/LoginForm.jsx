import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../Style/login.css';
import { connect } from 'react-redux';
import { userActions } from '../redux/actions/userActions';


class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.props.dispatch(userActions.logout());

    this.state = {
      email: '',
      password: '',
      submitted: false,
    };
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email && password) {
      dispatch(userActions.login(email, password));
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-6 col-md-4 col-md-offset-4">
            <h1 className="text-center login-title">Veuillez vous connecter</h1>
            <div className="account-wall">
              <img
                className="profile-img"
                src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                alt=""
              />
              <form className="form form-signin" onSubmit={e => this.onSubmit(e)}>
                <input
                  onChange={e => this.onChange(e)}
                  name="email"
                  type="text"
                  className="form-control"
                  placeholder="Email"
                  required
                  autoFocus
                />
                <input
                  onChange={e => this.onChange(e)}
                  name="password"
                  type="password"
                  className="form-control"
                  placeholder="Password"
                  required
                />
                <button className="btn btn-primary" type="submit">Se connecter</button>
              </form>
            </div>
            <Link to="/register">Inscrivez-vous</Link>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { loggingIn } = state.authentication;
  return {
    loggingIn,
  };
}

export default connect(mapStateToProps)(LoginForm);
